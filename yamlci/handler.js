"use strict"

let parameters = process.env['Http_Path']
//⚠️TODO: check ... but it'a demo
let function_name = parameters.split("/")[1]
let language = parameters.split("/")[2]

let deploy_jobs = (function_name, language) => `
build_${function_name}:
  extends: .production_build
  variables: 
    FUNCTION_NAME: "${function_name}"
    LANGUAGE: "${language}"

push_${function_name}:
  extends: .production_push
  variables: 
    FUNCTION_NAME: "${function_name}"

deploy_${function_name}:
  extends: .production_deploy
  variables: 
    FUNCTION_NAME: "${function_name}"

preview_build_${function_name}:
  extends: .review_build
  variables: 
    FUNCTION_NAME: "${function_name}"
    LANGUAGE: "${language}"

preview_push_${function_name}:
  extends: .review_push
  variables: 
    FUNCTION_NAME: "${function_name}"

preview_deploy_${function_name}:
  extends: .review_deploy
  variables: 
    FUNCTION_NAME: "${function_name}"

preview_stop_${function_name}:
  extends: .review_stop
  variables: 
    FUNCTION_NAME: "${function_name}"
`

module.exports = (context, callback) => {
  callback(undefined, deploy_jobs(function_name, language));
}
